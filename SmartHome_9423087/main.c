/**
  ******************************************************************************
  * @Author         [Shamisa Kaspour]
  * @StudentNumber  [9423087]
  * @ProjectName    Smart Home
  * @Date           22-January-2018
  * @brief          Main program body
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"

/** @addtogroup Template_Project
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
void init_gpio(void);
void delay(uint64_t);
float average(float array[]);

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{	
	/* variables */
	float Temperature = 0;
	float Light = 0;
	float Humidity = 0;
	uint32_t DayorNight = 0;
	float Temps[10] = {0};
	float TempAVG = 0;
  float Humids[10] = {0};
  float HumidAVG = 0;
  float Lights[10] = {0};
  float LightAVG = 0;
	uint16_t temporary = 0;
 	int i = 0;
	
	
	/* application code */
	init_gpio();

  /* Infinite loop */
  while (1)
  {
		/*store variables*/
		temporary = GPIOA->ODR;                        
		temporary &= 0x000000FF; 
		Temperature = ((float)(temporary))/2 - 38; //temperature mapping
		
		temporary = GPIOA->ODR;
		temporary = (temporary & 0x0000FF00) >> 8;
		Light = temporary * 32; //light mapping
		
		temporary = GPIOB->ODR;
		temporary &= 0x000000FF;
		Humidity = ((float)(temporary)) / 2; //humidity mapping
		
		DayorNight = GPIOB->ODR; //day and night detection              
		DayorNight &= 0x00000100;
		
		/*average setting*/
		TempAVG = average(Temps);
		HumidAVG = average(Humids);
		LightAVG = average(Lights);
		
		/* Temperature detection*/
		if(Temperature > 15 && TempAVG < 15)
		{		
			GPIOC->IDR &= 0xFFFFFFFC; 
			GPIOC->IDR |= 0x00000001; //turn off the heating system		
		}
		
		if(Temperature > 30 && TempAVG < 30)
		{		
			GPIOC->IDR &= 0xFFFFFFFC;  
			GPIOC->IDR |= 0x00000002; //the coling system works slow		
		}
		
		if(Temperature > 45 && TempAVG < 45)
		{		
			GPIOC->IDR |= 0x00000003; //the cooling system works fast
		}
		
		if(Temperature < 40 && TempAVG > 40)
		{		
			GPIOC->IDR &= 0xFFFFFFFC;
			GPIOC->IDR |= 0x00000002; //the coling system works slow 		
		}
		
		if(Temperature < 25 && TempAVG > 25)
		{		
			GPIOC->IDR &= 0xFFFFFFFC;
			GPIOC->IDR |= 0x00000001; //turn off the cooling system		
		}
		
		if(Temperature > 10 && TempAVG < 10)
		{		
			GPIOC->IDR &= 0xFFFFFFFC; //turn on the heating system
		}
		
		/* bulb & curtain setting */
		if(DayorNight == 0) //night
		{  
			if(Light > 2000 && LightAVG < 2000)
			{
				GPIOC->IDR &= 0xFFFFFFF3;
			  GPIOC->IDR |= 0x00000008; //turn on two bulbs
			}
			
      if(Light > 4000 && LightAVG < 4000)
			{
				GPIOC->IDR &= 0xFFFFFFF3;
				GPIOC->IDR |= 0x00000006; //turn on one bulb
			}
			
			if(Light > 6000 && LightAVG < 6000)
			{
				GPIOC->IDR &= 0xFFFFFFF3; //turn off all bulbs
			}
			
			if ( Light < 5000 && LightAVG > 5000)
			{
				GPIOC->IDR &= 0xFFFFFFF3;
				GPIOC->IDR |= 0x00000006; //turn on one bulb	
			}
			
			if(Light < 3000 && LightAVG > 3000)
			{
				GPIOC->IDR &= 0xFFFFFFF3;
				GPIOC->IDR |= 0x00000008; //turn on two bulbs
			}
			
			if(Light < 1000 && LightAVG > 1000)
			{
				GPIOC->IDR |= 0x0000000C; //turn on three bulbs
			}
		}
		else
		{
			if(Light > 2000 && LightAVG < 2000)
			{
				GPIOC->IDR &= 0xFFFFFFCF;
			  GPIOC->IDR |= 0x00000020;
			}
			
      if(Light > 4000 && LightAVG < 4000)
			{
				GPIOC->IDR &= 0xFFFFFFCF;
				GPIOC->IDR |= 0x00000010;
			}
			
			if(Light > 6000 && LightAVG < 6000)
			{
				GPIOC->IDR &= 0xFFFFFFCF;
			}
			
			if(Light < 5000 && LightAVG > 5000)
			{
				GPIOC->IDR &= 0xFFFFFFCF;
				GPIOC->IDR |= 0x00000010;	
			}
			
			if(Light < 3000 && LightAVG > 3000)
			{
				GPIOC->IDR &= 0xFFFFFFCF;
				GPIOC->IDR |= 0x00000020;
			}
			
			if(Light < 1000 && LightAVG > 1000)
			{
				GPIOC->IDR |= 0x00000030;
			}
		}
		/* humidity setting */
		if(Light > 2000 && LightAVG < 2000)
		{
			GPIOC->IDR &= 0xFFFFFF3F;
		  GPIOC->IDR |= 0x00000080;
		}
			
    if(Humidity > 50 && HumidAVG < 50)
		{
			GPIOC->IDR &= 0xFFFFFF3F;
			GPIOC->IDR |= 0x00000060;
		}
			
		if(Humidity > 75 && HumidAVG < 75)
		{
		  GPIOC->IDR &= 0xFFFFFF3F;
		}
			
		if(Humidity < 65 && HumidAVG > 65)
		{
			GPIOC->IDR &= 0xFFFFFF3F;
			GPIOC->IDR |= 0x00000060;	
		}
			
		if(Humidity < 40 && HumidAVG > 40)
		{
			GPIOC->IDR &= 0xFFFFFF3F;
			GPIOC->IDR |= 0x00000080;
		}
			
		if(Humidity < 15 && HumidAVG > 15)
		{
			GPIOC->IDR |= 0x000000C0;
		}
			
		/* store temperature & light & humidity in array and set i equal to 0 if i reach to 10*/	
		if(i == 10) 
		{
			i = 0;
		}
		Temps[i] = Temperature;
		Lights[i] = Light;
		Humids[i] = Humidity;
		
		i++;
				
		delay(5);
  }	
}

void init_gpio(void)
{
	RCC->AHB1ENR |= 0x0000001F;     //Enable GPIOA, GPIOB, GPIOC, GPIOD and GPIOE clock in AHB1

  /* port registers initialization */	
	GPIOA->MODER = 0x00000000;	
	GPIOA->OSPEEDR = 0x55555555;
	GPIOA->PUPDR = 0x00000000;

	GPIOB->MODER = 0x00000000;
	GPIOB->OSPEEDR = 0xAAAAAAAA;
	GPIOB->PUPDR = 0x00000000;
	
	GPIOC->MODER = 0x55555555;
	GPIOC->OTYPER &= 0xFFFF0000;
	GPIOC->OSPEEDR = 0xFFFFFFFF;
	GPIOC->PUPDR = 0x55555555;
	
	GPIOD->MODER = 0x55555555;
	GPIOD->OTYPER |= 0x0000FFFF;
	GPIOD->OSPEEDR = 0x00000000;
	GPIOD->PUPDR = 0xAAAAAAAA;
	
	GPIOE->MODER = 0xFFFFAAAA;
	GPIOE->OSPEEDR = 0xAAAAFFFF;
	GPIOE->PUPDR = 0x00000000;
}

void delay(uint64_t n)
{
	n *= 1000000; 
	while(n != 0){n -= 1;}
}

/*average of temperature & light & humidity*/
float average(float array[])
{
		float AVG = 0;
	  int j = 0;
		for (j = 0; j < 10; j++)
		{
			AVG += array[j];
		}
		
		AVG /= 10;
		return AVG;
}

